package edu.sjsu.android.project4tonyxie;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener,
        GoogleMap.OnMapLongClickListener, LoaderManager.LoaderCallbacks<Cursor>,
        SharedPreferences.OnSharedPreferenceChangeListener {

    private GoogleMap mMap;
    private final LatLng LOCATION_UNIV = new LatLng(37.335371, -121.881050);
    private final LatLng LOCATION_CS = new LatLng(37.333714, -121.881860);
    private Uri uri = MapProvider.CONTENT_URI;
    private Marker markUnvi = null;
    private Marker markCS = null;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        LoaderManager.getInstance(this).restartLoader(0, null, this);
        sp = getPreferences(MODE_PRIVATE);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(sp.getInt("map_key", GoogleMap.MAP_TYPE_NORMAL));
        float lat = sp.getFloat("current_lat", 0);
        float lng = sp.getFloat("current_long", 0);
        float zoom = sp.getFloat("current_zoom", 0);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), zoom));
        mMap.setOnMapClickListener(this);
        mMap.setOnMapLongClickListener(this);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mMap.addMarker(new MarkerOptions().position(latLng));

        ContentValues values = new ContentValues();
        values.put("latitude", latLng.latitude);
        values.put("longitude", latLng.longitude);
        values.put("zoomLevel", mMap.getCameraPosition().zoom);

        // Toast message if successfully inserted
        new Operations().execute(values);
        Toast.makeText(this, "Marker Added", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        mMap.clear();
        new Operations().execute((ContentValues) null);
        Toast.makeText(this,"All Markers Deleted",Toast.LENGTH_SHORT).show();
    }

    public void getLocation(View view){
        GPSTracker tracker = new GPSTracker(this);
        tracker.getLocation();
    }

    public void switchView(View view) {
        CameraUpdate update = null;
        if(markUnvi != null){
            markUnvi.remove();
        }
        if(markCS != null){
            markCS.remove();
        }
        if (view.getId() == R.id.city) {
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            markUnvi = mMap.addMarker(new MarkerOptions().
                    position(LOCATION_UNIV).title("Find SJSU here!"));
            update = CameraUpdateFactory.newLatLngZoom(LOCATION_UNIV, 10f);
        }else if(view.getId() == R.id.univ){
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            markUnvi = mMap.addMarker(new MarkerOptions().
                    position(LOCATION_UNIV).title("Find SJSU here!"));
            update = CameraUpdateFactory.newLatLngZoom(LOCATION_UNIV, 14f);
        } else if (view.getId() == R.id.cs) {
            mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            markCS = mMap.addMarker(new MarkerOptions().
                    position(LOCATION_CS).title("Find CS Department Here!"));
            update = CameraUpdateFactory.newLatLngZoom(LOCATION_CS, 18f);
        }
        mMap.animateCamera(update);
    }

    public void uninstall(View view){
        Intent delete = new Intent(Intent.ACTION_DELETE,
                Uri.parse("package:" + getPackageName()));
        startActivity(delete);
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        return new CursorLoader(this, uri, null,
                null, null, null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        if(data != null && data.moveToFirst()){
            do{
                mMap.addMarker(new MarkerOptions().position(new LatLng(
                        data.getDouble(1),
                        data.getDouble(2))));
            }while(data.moveToNext());
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

    }

    @Override
    public void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("map_key", mMap.getMapType());
        editor.putFloat("current_lat", (float) mMap.getCameraPosition().target.latitude);
        Toast.makeText(this, String.valueOf((float) mMap.getCameraPosition().target.latitude), Toast.LENGTH_SHORT).show();
        editor.putFloat("current_long", (float) mMap.getCameraPosition().target.longitude);
        editor.putFloat("current_zoom", mMap.getCameraPosition().zoom);
        editor.apply();
    }

    class Operations extends AsyncTask<ContentValues, Void, Void> {

        @Override
        protected Void doInBackground(ContentValues... contentValues) {
            if(contentValues[0] != null){
                getContentResolver().insert(uri, contentValues[0]);
            }else{
                getContentResolver().delete(uri, null, null);
            }
            return null;
        }
    }
}